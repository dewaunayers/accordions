import { get, isFunction } from "lodash";
import * as React from "react";
import { useEffect } from "react";

import {
  MAIN_UPDATE,
  PARENT_CLOSE,
  CHILD_TOGGLE,
  ADJUST_PARENT_HEIGHT,
} from "../constants";

export const withBase = (Component: React.ComponentType<any>) => ({
  observer,
  children,
  id,
  ...restProps
}: any) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const staticOpenHeight = React.useRef(0);
  const [openHeight, setOpenHeight] = React.useState(0);

  const onMainControllerUpdate = (args: any) => {
    setIsOpen(args.isOpen);
    observer.notify(CHILD_TOGGLE, {
      id,
      staticHeight: staticOpenHeight.current,
      state: args.isOpen,
    });
  };

  const onParentClose = (args: any) => {
    if (args.id !== id && id.indexOf(args.id) > -1) {
      setIsOpen(false);
    }
  };

  const adjustParentHeight = (args: any) => {
    if (args.id !== id && args.id.indexOf(id) > -1) {
      console.log(args.adjustment, openHeight, staticOpenHeight);
      setOpenHeight(staticOpenHeight + args.adjustment);
    }
  };

  const onChildToggle = (args: any) => {
    if (args.id !== id && args.id.indexOf(id) > -1) {
      console.log("child is doing a thing", args.id, id, args.state);
      if (args.state) {
        observer.notify(ADJUST_PARENT_HEIGHT, {
          ...args,
          adjustment: args.staticHeight,
        });
      } else {
        observer.notify(ADJUST_PARENT_HEIGHT, {
          ...args,
          adjustment: args.staticHeight * -1,
        });
      }
    }
  };

  const handleController = () => {
    const nextOpenState = !isOpen;
    setIsOpen(nextOpenState);
    observer.notify(CHILD_TOGGLE, {
      id,
      staticHeight: staticOpenHeight.current,
      state: nextOpenState,
    });
    if (nextOpenState === false) {
      observer.notify(PARENT_CLOSE, { id });
    }
  };

  const [contentRef, setContentRef] = React.useState();
  useEffect(() => {
    // will update normally when the ref changes
    if (contentRef !== undefined) {
      console.log(contentRef.scrollHeight);
      setOpenHeight(contentRef.scrollHeight);
      staticOpenHeight.current = contentRef.scrollHeight;
    }
  }, [contentRef]);

  useEffect(() => {
    observer.attach(MAIN_UPDATE, onMainControllerUpdate);
    observer.attach(PARENT_CLOSE, onParentClose);
    observer.attach(CHILD_TOGGLE, onChildToggle);
    observer.attach(ADJUST_PARENT_HEIGHT, adjustParentHeight);
    return () => {
      observer.detach(onMainControllerUpdate);
      observer.detach(onParentClose);
      observer.detach(onChildToggle);
      observer.detach(adjustParentHeight);
    };
  }, []);

  useEffect(() => observer.notify("UPDATE_COUNT", isOpen), [isOpen]);

  return (
    <Component
      id={id}
      isOpen={isOpen}
      handleController={handleController}
      contentRef={(ref) => setContentRef(ref)}
      openHeight={openHeight}
      {...restProps}
    >
      {isFunction(children) ? children({ observer }) : children}
    </Component>
  );
};
