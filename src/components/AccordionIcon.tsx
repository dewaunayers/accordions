import * as React from "react";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import { IconTransitionContainer } from "../accordion.styles";

interface IAccordionIcon {
  id: string;
  isOpen: boolean;
  openIcon?: any;
  closeIcon?: any;
}

export const AccordionIcon: React.FC<IAccordionIcon> = ({
  id,
  isOpen,
  openIcon,
  closeIcon,
}) => {
  const isMounting = React.useRef(true);
  const OpenIcon = openIcon || KeyboardArrowUpIcon;
  const CloseIcon = closeIcon || KeyboardArrowDownIcon;
  const [Icon, setIcon] = React.useState(CloseIcon);
  const [animationClass, setAnimationClass] = React.useState("");

  React.useEffect(() => {
    if (isMounting.current) {
      isMounting.current = false;
    } else {
      console.log(isOpen);
      setAnimationClass(isOpen ? "rotate" : "reverse");
      requestAnimationFrame(() => {
        setTimeout(() => {
          setAnimationClass("");
          setIcon(isOpen ? OpenIcon : CloseIcon);
        }, 250);
      });
    }
  }, [isOpen]);
  return (
    <IconTransitionContainer className={animationClass}>
      <Icon />
    </IconTransitionContainer>
  );
};
