import * as React from "react";
import Observer from "../Observer";
import { AccordionHeaderComponent } from "./AccordionHeader";
import { MAIN_UPDATE } from "../constants";

const initialState = { count: 0 };

function reducer(state: typeof initialState, action: { type: string }) {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    default:
      throw new Error();
  }
}

export const AccordionController: React.FunctionComponent<any> = ({
  id,
  children,
}) => {
  const isMounting = React.useRef(true);
  const AccordionControllerObserver = React.useMemo(() => new Observer(), []);
  const [isOpen, setIsOpen] = React.useState(false);
  const [touched, setTouched] = React.useState(false);
  const [action, setAction] = React.useState("expand");
  const [state, dispatch] = React.useReducer(reducer, initialState);

  const handleController = () => {
    setIsOpen(!isOpen);
    setTouched(true);
  };

  const onChildChange = (isChildOpen: boolean) => {
    if (isChildOpen) {
      dispatch({ type: "increment" });
    } else {
      dispatch({ type: "decrement" });
    }
  };

  React.useEffect(() => {
    AccordionControllerObserver.attach("UPDATE_COUNT", onChildChange);
  }, []);

  React.useEffect(() => {
    if (isMounting.current) {
      isMounting.current = false;
    } else {
      if (touched) {
        AccordionControllerObserver.notify(MAIN_UPDATE, { isOpen });
        setTouched(false);
      }
    }
  }, [isOpen]);

  React.useEffect(() => {
    if (state.count !== AccordionControllerObserver.size(MAIN_UPDATE)) {
      setAction("expand");
      setIsOpen(false);
    } else {
      setAction("collapse");
      setIsOpen(true);
    }
  }, [state]);

  return (
    <div id={id}>
      <AccordionHeaderComponent
        content={"Main Accordion"}
        handleController={handleController}
        withComponent="h3"
        ctaContent={`${action === "expand" ? "Expand" : "Collapse"} all`}
      />
      {children({ observer: AccordionControllerObserver, onChildChange })}
    </div>
  );
};
