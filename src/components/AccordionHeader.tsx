import * as React from "react";
import styled from "@emotion/styled";
import { AccordionHeader, CallToActionButton } from "../accordion.styles";

export const AccordionHeaderComponent: React.FunctionComponent<any> = ({
  withComponent,
  handleController,
  content,
  ctaContent,
}) => {
  let TitleComponent = styled.h3``;
  if (withComponent) {
    TitleComponent = TitleComponent.withComponent(withComponent);
  }
  return (
    <AccordionHeader>
      <TitleComponent>{content}</TitleComponent>
      <CallToActionButton onClick={handleController}>
        {ctaContent}
      </CallToActionButton>
    </AccordionHeader>
  );
};
