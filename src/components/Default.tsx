import * as React from "react";
import { AccordionContainer, AccordionBody } from "../accordion.styles";
import { AccordionIcon } from "./AccordionIcon";
import { AccordionHeaderComponent } from "./AccordionHeader";
import { withBase } from "./withBase";

const Component: React.FunctionComponent<any> = ({
  id,
  isOpen,
  handleController,
  children,
  contentRef,
  openHeight,
  title,
  titleTag = "h2",
  openIcon,
  closeIcon,
}) => {
  return (
    <AccordionContainer id={id}>
      <AccordionHeaderComponent
        content={title}
        withComponent={titleTag}
        handleController={handleController}
        ctaContent={
          <AccordionIcon
            id={`${id}-icon`}
            isOpen={isOpen}
            openIcon={openIcon}
            closeIcon={closeIcon}
          />
        }
      />
      <AccordionBody
        className={isOpen ? "expanded" : "collapsed"}
        ref={(ref) => contentRef(ref)}
        openHeight={`${openHeight}px`}
      >
        {children}
      </AccordionBody>
    </AccordionContainer>
  );
};

export const DefaultAccordion = withBase(Component);
