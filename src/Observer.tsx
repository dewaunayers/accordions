export type TObserver = { type: string; observer: (...args: any) => void };
export type TObserverClass = typeof Observer;

export default class Observer {
  private observers: TObserver[] = [];

  public attach(type: string, observer: TObserver["observer"]) {
    this.observers.push({ type, observer });
  }

  public detach(observer: TObserver["observer"]) {
    this.observers = this.observers.filter(obs => obs.observer === observer);
  }

  public notify(type: string, args: any) {
    this.observers.forEach(obs => obs.type === type && obs.observer(args));
  }

  public size(type: string) {
    return this.observers.filter(obs => obs.type === type).length;
  }
}
