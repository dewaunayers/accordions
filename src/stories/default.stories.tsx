import * as React from "react";
import { Accordion } from "../";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";

export default {
  title: "Accordion",
  component: Accordion,
};

const Template = (args: any) => (
  <Accordion id="acc_controller" controller>
    {({ observer, onChildChange }: any) => {
      return (
        <>
          <Accordion
            id="acc1"
            title="Accordion 1"
            observer={observer}
            onUpdate={onChildChange}
            default
          >
            Content <button>tabbable item</button>
            <a href="https://www.google.com" target="_blank">
              Google it...
            </a>
            <Accordion
              id="acc1_1"
              title="Accordion 1.1"
              titleTag="h3"
              observer={observer}
              onUpdate={onChildChange}
              default
            >
              Nested Content 1 <button>tabbable item</button>
            </Accordion>
            <Accordion
              id="acc1_2"
              title="Accordion 1.2"
              titleTag="h3"
              observer={observer}
              onUpdate={onChildChange}
              default
            >
              Nested Content 1.2 <button>tabbable item</button>
            </Accordion>
          </Accordion>
          <Accordion
            id="acc2"
            title={<span style={{ color: "orange" }}>Accordion 2</span>}
            observer={observer}
            onUpdate={onChildChange}
            default
          >
            Content <button>tabbable item</button>
            <Accordion
              id="acc2_1"
              title="Accordion 2.1"
              titleTag="h3"
              observer={observer}
              onUpdate={onChildChange}
              default
            >
              Nested Content 2 <button>tabbable item</button>
            </Accordion>
            <Accordion
              id="acc2_2"
              title="Accordion 2.2"
              titleTag="h3"
              observer={observer}
              onUpdate={onChildChange}
              openIcon={RemoveIcon}
              closeIcon={AddIcon}
              default
            >
              Nested Content 2.2 <button>tabbable item</button>
            </Accordion>
          </Accordion>
        </>
      );
    }}
  </Accordion>
);

export const Default = Template.bind({});
