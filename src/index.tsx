import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import * as React from "react";
import { AccordionController } from "./components/AccordionController";
import { DefaultAccordion } from "./components/Default";
import { TObserverClass } from "./Observer";

const variantPicker = (props: any) => {
  if (props.controller) return AccordionController;
  if (props.default) return DefaultAccordion;
  if (props.list) return DefaultAccordion;
  if (props.pill) return DefaultAccordion;
};

interface TAccordion {
  id: string;
  title?: string | JSX.Element;
  titleTag?: string;
  openIcon?: string | JSX.Element | OverridableComponent<any>;
  closeIcon?: string | JSX.Element | OverridableComponent<any>;
  onUpdate?: Function;
  observer?: TObserverClass;
  controller?: boolean;
  default?: boolean;
  list?: boolean;
  pill?: boolean;
  collapsible?: boolean;
}

export const Accordion: React.FunctionComponent<TAccordion> = (props) => {
  const AccordionComponent = variantPicker(props);

  return <AccordionComponent {...props} />;
};
