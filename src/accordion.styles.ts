import styled from "@emotion/styled";

export const Box = styled.div``;

const ButtonReset = styled.button`
  border: none;
  margin: 0;
  padding: 0;
  width: auto;
  overflow: visible;
  background: transparent;
  color: inherit;
  font: inherit;
  line-height: normal;

  /* Corrects font smoothing for webkit */
  -webkit-font-smoothing: inherit;
  -moz-osx-font-smoothing: inherit;
  -webkit-appearance: none;

  &::-moz-focus-inner {
    border: 0;
    padding: 0;
  }
`;

export const CallToActionButton = styled(ButtonReset)`
  height: 36px;
  width: 36px;
`;

export const IconTransitionContainer = styled.div`
  &.rotate {
    animation: rotate 0.5s;
  }

  &.reverse {
    animation: reverseRotate 0.5s;
  }

  @keyframes rotate {
    from {
      transform: rotate(0);
    }

    to {
      transform: rotate(-180deg);
    }
  }

  @keyframes reverseRotate {
    from {
      transform: rotate(0);
    }

    to {
      transform: rotate(180deg);
    }
  }
`;

export const AccordionContainer = styled.div`
  border: 1px solid grey;
`;

export const AccordionHeader = styled(Box)`
  display: flex;
  padding: 1rem;
  justify-content: space-between;
`;

export const AccordionBody = styled.div<{ openHeight: string }>`
  display: block;
  overflow: hidden;
  padding: 1rem;

  * {
    visibility: visible;
  }

  &.expanded,
  &.collapsed {
    transition: all 0.3s ease-in-out;
  }

  &.expanded {
    height: ${(props) => props.openHeight};
  }

  &.collapsed {
    height: 0;
    padding: 0;

    * {
      visibility: hidden;
    }
  }
`;
